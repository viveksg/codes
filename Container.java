import java.io.*;
import java.util.*;

class Container{
    public static void main(String args[])throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int nums = Integer.parseInt(reader.readLine()), i = 0, wt = 0, container = 0;
        StringTokenizer st = new StringTokenizer(reader.readLine());
        int [] weights = new int[10001]; // max weight can be 10000,for given producs in range [1,100000]
        for(i = 0;i < nums; i++){
            wt = Integer.parseInt(st.nextToken());
            weights[wt] = 1;
        }

        for(i = 0;i < weights.length; i++){
             if(weights[i]> 0){
                 container++;
                 i = i + 4;
             }
        }
        System.out.println(container);
    }
 
}

/*
This solution focus on maintains a "weights" array 
weights[i] = 1 if there exits any container, with weight "i"
For example if we have 4 containers with weights 23,343,641,9898 then
weight[23] = 1
weight[343] = 1
weight[641] = 1
weight[9898] = 1

From here we just need to parse array weights.
In every iteration we just if weight i is set to 1.
And if thats the case we increment the container and increment i = i+4
so in next iteration i is actually i + 5.

Tricky cases

10
1 1 1 1 1 1 1 1 1 1
Output
1

10
23 23 23 23 23 101 101 9000 9000 9000
Output
3
*/

