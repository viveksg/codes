import java.io.*;
import java.util.*;

class Clue{
    public static void main(String args[]) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String,Integer> words = new HashMap<String,Integer>();
        String sentence = "";
        int i = 0, j =0, k=0;
        boolean endCharacterNotFound = true, wordNotFound = true;
        String found = "$", currentWord = "", reverseWord = "";
        while(endCharacterNotFound && wordNotFound){
            sentence = reader.readLine().trim();
            for(i = 0;i < sentence.length(); i++){
                if(sentence.charAt(i) == '$'){
                     endCharacterNotFound = false;
                     break;
                }
                if(sentence.charAt(i) ==  ' '){
                    if(currentWord.length() > 0){
                        reverseWord = getReverseString(currentWord);
                        if(words.containsKey(reverseWord)){
                            wordNotFound = false;
                            found = reverseWord;
                            break;
                        }
                        else{
                            words.put(currentWord,1);
                            currentWord = "";
                        } 
                    }
                }
                else
                     currentWord += sentence.charAt(i);
            }
        }
        System.out.println(found);
    }

    public static String getReverseString(String s){
        String ret = "";
        int i = 0;
        for(i = s.length() -1;i > -1; i--)
            ret += s.charAt(i);
        return ret;
    }
}
